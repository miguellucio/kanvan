import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  //arrays
  tasks:any[] = [];
  toDo:any[] = [];
  data:any[] = [];
  data2:any[] = [];

  constructor() { 
    Swal.fire({
      icon: 'info',
      title: 'Arrasta las tareas',
      text: 'Para mover las tareas de lugar tienes que arrastrarlas a la siguiente accion, NO RECARGES LA PAGINA, DE LO CONTRARIO TUS TAREAS SE PERDERAN',
    });
  }

  ngOnInit(): void {
    
  }



  add_task(form:NgForm){

    if(form.valid){
      this.tasks.push(form.value);
      form.reset();
      console.log(this.tasks);
    }else{
      //alert('Por favor ingresa una tarea :)')
      Swal.fire({
        icon: 'info',
        title: 'Oops...',
        text: 'Antes de guardar agrega una tarea',
      })
    }

  }



  drop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }


  delete(i:number){
    this.data2.splice(i, 1);
  }


  

}
